# runTest.sh
Lance les tests unitaires des scripts de backups.
__Nécéssite docker d'installé.__

Ce script execute backup backup_nextcloud.sh dans une _sandbox_ monté dans un conteneur docker. 

# Le repertoire sandbox/ 
Il contient des données à monter à la racine du conteneur. 

# Le repertoire sandbox/fakedBin/ 
Il contient de faux executables accessible dans le PATH du conteneur pour une execution bouchonnée du script.
