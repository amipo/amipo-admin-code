#! /bin/sh -e

echo "------------"
DATE_START=`date +%F_%s`

BACKUP_CONTAINER_DIR="/backups/nextcloud"
BACKUP_DIR="$BACKUP_CONTAINER_DIR/$DATE_START"
LATEST_LINK="$BACKUP_CONTAINER_DIR/latest"
NUAGE_APP_DIR="/var/www/prod/nuage"
NUAGE_DATA_DIR="/var/nextcloud/data"
NUAGE_LOGS_DIR="/var/log/nextcloud"

BASE_DIR="$(dirname $(readlink -f $0))"
PHP_OCC_CMD="php $NUAGE_APP_DIR/occ"


errorCount=0

echo "[STARTING] Backup NextCloud ${DATE_START} on `date`"

echo "Create backup directory..."
mkdir -p $BACKUP_DIR

echo "Maintenance mode: ON"
$PHP_OCC_CMD maintenance:mode --on

echo "== Backing up database... =="
SHEMA_DUMP_ARCHIVE_FILEPATH="$BACKUP_DIR/schema-nextcloud-dump.psql.gz"
DATA_DUMP_ARCHIVE_FILEPATH="$BACKUP_DIR/data-nextcloud-dump.custom"
pg_dump --username=nextcloud_backup -d nextcloud -h localhost --schema-only -n nextcloud | gzip > $SHEMA_DUMP_ARCHIVE_FILEPATH || errorCount=$(( errorCount + 1 ))
pg_dump --username=nextcloud_backup -d nextcloud -h localhost --data-only -n nextcloud -Fc > $DATA_DUMP_ARCHIVE_FILEPATH || errorCount=$(( errorCount + 1 ))
echo "Database dump:"
ls -lh $SHEMA_DUMP_ARCHIVE_FILEPATH $DATA_DUMP_ARCHIVE_FILEPATH

echo "== Copying data files... =="
rsync -Aax --stats --human-readable --exclude="/updater*" "$NUAGE_DATA_DIR/" "$BACKUP_DIR/data/" || errorCount=$(( errorCount + 1 ))

$BASE_DIR/restore_nextcloud_maintenance.sh

echo "[ENDING] Backup of NextCloud finished on `date`"


echo "[STARTING] Backup extra ${DATE_START} on `date`"

echo "== Copying all Nextcloud App files... =="
rsync -Aax --stats --human-readable "$NUAGE_APP_DIR/" "$BACKUP_DIR/app/" || errorCount=$(( errorCount + 1 ))

echo "== Copying prod log files... =="
rsync -Aax --stats --human-readable --include="/nuage-prod.log*" --exclude="*" "$NUAGE_LOGS_DIR/" "$BACKUP_DIR/logs/" || errorCount=$(( errorCount + 1 ))

#echo "== Copying nginx config files... =="
#rsync -Aax --stats --human-readable "/etc/nginx/" "$BACKUP_DIR/etc_nginx/" || errorCount=$(( errorCount + 1 ))

echo "== Copying etckeeper backup git repo... =="
rsync -Aax --stats --human-readable /backups/etckeeper-git "$BACKUP_DIR/etckeeper-git/" || errorCount=$(( errorCount + 1 ))

echo "[ENDING] Backup extra finished on `date`"


echo "[STARTING] Archiving backup ${DATE_START} on `date`"

if [ "$errorCount" -gt 0 ]
then 
    BACKUP_ARCHIVE="$BACKUP_DIR-avec_${errorCount}_erreurs.tar.gz"
else
    BACKUP_ARCHIVE="$BACKUP_DIR.tar.gz"
fi

echo "== Build a compressed archive... =="
cd $( dirname $BACKUP_DIR )
tar -czf $BACKUP_ARCHIVE $( basename $BACKUP_DIR )

test -L $LATEST_LINK && rm $LATEST_LINK
ln -s $BACKUP_ARCHIVE $LATEST_LINK

echo "== Remove backup directory... =="
rm -rf -- $BACKUP_DIR

echo "== Removing old backups... =="
echo "Listing old archives:"
find $BACKUP_CONTAINER_DIR -maxdepth 1 -mtime +6

# Search files not links older than 6.5 days, and check if there is at least 7 files before delete old files. Wont delete anything if there is less than 7 files.
find $BACKUP_CONTAINER_DIR/* -maxdepth 0 ! -type l -mmin +$(( 1440 * 13/2 )) -exec /bin/sh -c "find $BACKUP_CONTAINER_DIR/* -maxdepth 0 -type f | head -n -7 | grep $BACKUP_CONTAINER_DIR > /dev/null && echo 'Removing old archive:' && ls -ldh {} && rm -rf -- {}" \;

echo "Backup directory content:"
ls -lh $BACKUP_CONTAINER_DIR

echo "Backup volume usage:"
df -h $BACKUP_CONTAINER_DIR

echo "[ENDING] Archiving backup finished on `date`"
