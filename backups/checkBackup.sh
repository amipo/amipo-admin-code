#! /bin/sh

BACKUP_CONTAINER_DIR="/backups/nextcloud"
LATEST_LINK="$BACKUP_CONTAINER_DIR/latest"

koMsg() {
	echo >&2 "\033[0;31mKO\033[0m"
}

echo "T:: Check latest link exists"
test -L "$LATEST_LINK" || koMsg

echo "T:: Check latest link pointe sur un fichier"
test -f "$LATEST_LINK" || koMsg

ARCHIVE_NAME="$( basename $(readlink -f $LATEST_LINK ) .tar.gz )"
echo "Try to inspect archive $ARCHIVE_NAME"

echo "T:: Try to list archive"
( tar -tf "$LATEST_LINK" "$ARCHIVE_NAME/data/" | grep "/data/" || koMsg ) | head > /dev/null

echo "T:: Check absence of inexisting dir"
( tar -tf "$LATEST_LINK" "$ARCHIVE_NAME/foo" && koMsg ) | head > /dev/null

echo "T:: KOCheck témoin: presence of inexisting dir"
( tar -tf "$LATEST_LINK" "$ARCHIVE_NAME/foo" 2>&1 || koMsg ) | head > /dev/null

echo "T:: Check presence of nextcloud schema dump"
( tar -tvf "$LATEST_LINK" "$ARCHIVE_NAME/schema-nextcloud-dump.psql.gz" || koMsg ) | head

echo "T:: Check presence of data dump"
( tar -tvf "$LATEST_LINK" "$ARCHIVE_NAME/data-nextcloud-dump.custom" || koMsg ) | head

echo "T:: Check presence of nextcloud data"
( tar -tf "$LATEST_LINK" "$ARCHIVE_NAME/data" || koMsg ) | head > /dev/null

echo "T:: Check presence of app data"
( tar -tf "$LATEST_LINK" "$ARCHIVE_NAME/app" || koMsg ) | head > /dev/null

echo "T:: Check presence of logs data"
( tar -tf "$LATEST_LINK" "$ARCHIVE_NAME/logs" || koMsg ) | head > /dev/null

#echo "T:: Check presence of nginx data"
#( tar -tf "$LATEST_LINK" "$ARCHIVE_NAME/etc_nginx" || koMsg ) | head > /dev/null

echo "T:: Check presence of etckeeper data"
( tar -tf "$LATEST_LINK" "$ARCHIVE_NAME/etckeeper-git" || koMsg ) | head > /dev/null
