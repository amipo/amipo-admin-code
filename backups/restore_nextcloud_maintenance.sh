#! /bin/sh

PHP_OCC_CMD="php /var/www/prod/nuage/occ"

echo "Maintenance mode: OFF"
$PHP_OCC_CMD maintenance:mode --off
echo "Nextcloud should be running: ON"
