#! /bin/sh

scriptDir="$( dirname $( readlink -f $0 ) )"
tag="$( echo $scriptDir | sed -e 's|/|_|g' | sed -e 's/^_//' )-sandbox"

echo "Building Dockerfile in $scriptDir with tag $tag"
docker build -t "$tag" "$scriptDir"

docker run $tag /bin/sh -c "/app/backup_nextcloud.sh ; /app/checkBackup.sh"
