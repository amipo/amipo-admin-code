#! /bin/sh -e

# Telecharge et dézipe l'artifact construit par le pipeline de génération du site web de prod
# Prend en paramètre le repertoire web dans lequel sera déployé le site web.
WEB_ROOT="$1"

DATE="$( date '+%F_%s' )"
DEST_DIR="$WEB_ROOT/amipo-site_$DATE"
ARTIFACTS_FILENAME="artifacts_$DATE.zip"
HASH_FILENAME="hash.txt"
SITE_PUB_DIR="$WEB_ROOT/html"
ARTIFACTS_URL="https://framagit.org/amipo/amipo-site/-/jobs/artifacts/master/download?job=prod"

rm -rf -- /tmp/amipo-site_* || true
tmpDir="$( mktemp -d /tmp/amipo-site_$DATE-XXXX )"

test -d "$WEB_ROOT" || ( echo "Les repertoire WEB_ROOT: [$WEB_ROOT] n'existe pas !" && false  )
! test -e "$SITE_PUB_DIR" || test -L "$SITE_PUB_DIR" || ( echo "Le fichier $SITE_PUB_DIR n'est pas un lien symbolique. La procédure est interrompue !" && false )

wget -O "$tmpDir/$ARTIFACTS_FILENAME" "$ARTIFACTS_URL"
sha1sum "$tmpDir/$ARTIFACTS_FILENAME" | awk '{ print $1 }' > "$tmpDir/$HASH_FILENAME"

# Check if downloaded archive is different than deployed one
! diff "$SITE_PUB_DIR/$HASH_FILENAME" "$tmpDir/$HASH_FILENAME" || ( echo "Pas de changement détecté." && false )

cd "$tmpDir"
unzip "$ARTIFACTS_FILENAME"
test -d "$tmpDir/public" || ( echo "Le repertoire public est manquant !" && false )
cp "$tmpDir/$HASH_FILENAME" "$tmpDir/public"

mv "$tmpDir" "$DEST_DIR"
test -L "$SITE_PUB_DIR" && rm -- "$SITE_PUB_DIR"

ln -s "$DEST_DIR/public" "$SITE_PUB_DIR"

