#! /bin/sh -e

# Publie le repertoire public/ du site web en production
# Prend en paramètre le repertoire chemin d'un repertoire public/ dans lequel est déployé le site web, exemple "/var/www/test/html"

usage() {
	echo >&2 "usage: $0 amipo-site_public_dir/"
	echo >&2 ""
	echo >&2 "       amipo-site_public_dir/: est un repertoire contenant le site web généré, comme par exemple \"/var/www/test/html\""
	echo ""
	exit 1
}

PUBLIC_DIR="$1"
PROD_DIR="/var/www/prod"
PROMOTION_DIR="$PROD_DIR/amipo-site-promoted_$( date +'%F_%s' )"

test -d "$PUBLIC_DIR" || ( echo "Le repertoire fourni $PUBLIC_DIR n'esiste pas !" && usage )
test -f "$PUBLIC_DIR/index.html" || ( echo "Le repertoire fourni $PUBLIC_DIR ne contient pas de fichier index.html !" && usage )

cp -r "$PUBLIC_DIR" "$PROMOTION_DIR"
chown -R www-data:www-data "$PROMOTION_DIR"

( test -L "$PROD_DIR/html" && rm "$PROD_DIR/html" ) || ( echo "Le fichier $PROMD/html n'est pas un lien symbolique ! Promotion en prod interrompue !" && false )
ln -s "$PROMOTION_DIR" "$PROD_DIR/html"
