#! /bin/sh -e

HOST="amipo.fr"

OK_200="^HTTP/[0-9\.]+ 200"
REDIRECT_301="^HTTP/[0-9\.]+ 301"
REDIRECT_302="^HTTP/[0-9\.]+ 302"
FORBIDEN_403="^HTTP/[0-9\.]+ 403"
NOT_FOUND_404="^HTTP/[0-9\.]+ 404"

RED='\033[0;31m'
GREEN='\033[0;32m'
NC='\033[0m'

OK_MSG="${GREEN}OK$NC"
KO_MSG="${RED}KO$NC"

checkUrl() {
	url="$1"
	status="$2"

	echo "checking $url for $status ..."
	( curl -I -s -w "%{http_code}" "$url" | egrep "$2" > /dev/null && echo $OK_MSG ) || echo $KO_MSG
}

echo "Test KO pour tester le cas KO"
checkUrl "https://$HOST" "$NOT_FOUND_404"


echo "Début des tests."

echo "\nChecking root and static ..."
checkUrl "http://$HOST" "$REDIRECT_302"
checkUrl "http://$HOST/" "$REDIRECT_302"
checkUrl "http://$HOST/foo" "$REDIRECT_302"
checkUrl "http://$HOST/static" "$REDIRECT_302"
checkUrl "http://$HOST/static/" "$NOT_FOUND_404"
checkUrl "http://$HOST/static/hello.html" "$OK_200"
checkUrl "http://$HOST/static/hello2.html" "$NOT_FOUND_404"

checkUrl "https://$HOST" "$OK_200"
checkUrl "https://$HOST/" "$OK_200"

echo "\nChecking nextcloud ..."
checkUrl "https://$HOST/nextcloud" "$NOT_FOUND_404"
checkUrl "https://$HOST/nextcloud/" "$NOT_FOUND_404"
checkUrl "https://$HOST/nuage" "$REDIRECT_302"
checkUrl "https://$HOST/nuage/" "$REDIRECT_302"
checkUrl "https://$HOST/nuage/login" "$OK_200"
checkUrl "http://$HOST/nuage/login" "$REDIRECT_302"

echo "\nChecking webdav and caldav ..."
checkUrl "https://$HOST/.well-known/carddav" "$REDIRECT_301"
checkUrl "https://$HOST/nuage/.well-known/carddav" "$FORBIDEN_403"
checkUrl "https://$HOST/.well-known/caldav" "$REDIRECT_301"
checkUrl "https://$HOST/nuage/.well-known/caldav" "$FORBIDEN_403"

echo "\nChecking nextcloud exta ..."
checkUrl "https://$HOST/.well-known/webfinger" "$OK_200"
checkUrl "https://$HOST/nuage/public.php?service=webfinger" "$OK_200"
checkUrl "https://$HOST/.well-known/host-meta" "$OK_200"
checkUrl "https://$HOST/nuage/public.php?service=host-meta" "$OK_200"
checkUrl "https://$HOST/.well-known/host-meta.json" "$OK_200"
checkUrl "https://$HOST/nuage/public.php?service=host-meta-json" "$OK_200"

echo "Ce test est terminé."
